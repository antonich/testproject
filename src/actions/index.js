import * as types from './actionstypes';

/*
Action Creators
*/
export function changeAppRoot(root) {
  return {
    type: types.ROOT_CHANGED, 
    root: root
  };
}

/*
dispatch the actionCreators 
*/
export function appInitialized() {
  return async function(dispatch, getState) {
    console.log("first is here!")
    // since all business logic should be inside redux actions
    // this is a good place to put your app initialization code
    dispatch(changeAppRoot('login'));
  };
}

export function login() {
  console.log("am i here??");
  return async function(dispatch, getState) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeAppRoot('after-login'));
  };
}
