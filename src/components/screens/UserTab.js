import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  AsyncStorage
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';
import * as  appActions from '../../actions/index';
import {connect} from 'react-redux';

var appjs = require('../../app.js');

class UserTab extends Component {

  async changeIsLoggedIn(key, value) {
    try {
          return await AsyncStorage.setItem(key, value);
      } catch (error) {
          console.error('AsyncStorage#setItem error: ' + error.message);
      }
  }

  async removeUserToken(key) {
    try {
          return await AsyncStorage.removeItem(key);
      } catch (error) {
          console.error('AsyncStorage#setItem error: ' + error.message);
      }
  }

  async _returnUserToken(){
    var token = await AsyncStorage.getItem('userToken');
    // console.log(token);
    return token
  }

  _logoutPress = () => {
    return AsyncStorage.getItem('userToken').then(
      (token) => {
        let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
        fetch('http://'+ address + ':8000/users/logout/', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token  '+ token, 
          }
        }).then((response) => {
          if (response.status != 200) {
            alert("User data problem.");
            return;
          } else {
            this.changeIsLoggedIn('isLoggedIn', 'False');
            this.removeUserToken('userToken');
            this.props.dispatch(appActions.appInitialized());
          }
        }).catch((error) => {
          return
        });
    }).catch((error) => {
      console.log(error)
      return
    });
  }


  _consoleUserToken() {
    const isLoggedIn = AsyncStorage.getItem('userToken').then(
      (value) => {
        console.log('User token = %s', value);
    }).catch((error) => {
      console.log("Error zone")
      return
    });
  }

  render() {
    return (
      <View style={styles.container}>
          <Button
            onPress={this._logoutPress.bind(this)}
            title="Log out"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
          <Button
            onPress={this._consoleUserToken.bind(this)}
            title="Get token"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
      </View>
    );
  }
}


export default connect()(UserTab);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  
});