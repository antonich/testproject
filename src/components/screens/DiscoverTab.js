import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  SectionList,
  Animated,
  AsyncStorage,
  FlatList,
  ActivityIndicator,
  NavigatorIOS
} from 'react-native';
import Navigation from 'react-native-navigation';
import PropTypes from 'prop-types';
import connect from 'react-redux';
import * as  appActions from '../../actions/index';
import { ListItem, Divider, Header } from 'react-native-elements'; // 0.18.5

export default class DiscoverTab extends Component {
  constructor(props) {
    super(props)
    //Be sure to add this line in the constructor, or the "this" in method _onRightButtonPress will reference to the object itself.
    // this._onRightButtonPress = this._onRightButtonPress.bind(this)
  }
  render() {
    return (
      <NavigatorIOS ref="nav"
        style = {styles.container}
        initialRoute= {{
          component: HometabPointer,
          title: "Navigation demo",
          rightButtonTitle: "Right Scene",
          onRightButtonPress: this._onRightButtonPress.bind(this)
        }}/>
    )
  }

  _onRightButtonPress() {
    this.refs.nav.push({
      title: "From Right",
      component: HometabPointer
    })
  }
}

class HometabPointer extends Component {
  constructor(props) {
      super(props);
  }
  render() {
    return (
      <View>
        <Text>Transform props: {this.props.showLable}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scene: {
    padding: 10,
    paddingTop: 74,
    flex: 1,
  }
})