/* @flow */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  SectionList,
  Animated,
  AsyncStorage,
  List,
  FlatList
} from 'react-native';
import Navigation from 'react-native-navigation';
import PropTypes from 'prop-types';
import connect from 'react-redux';
import * as  appActions from '../../actions/index';
import { ListItem, Divider, Header } from 'react-native-elements'; // 0.18.5

var appjs = require('../../app.js');

export default class Hometab extends Component {
  constructor(props) {
      super(props);

      this.state = {
        loading: false,
        data: [],
        error: null,
        scrollY: new Animated.Value(0)
    };
  }

  componentDidMount() {
    this.fetchFromServer();  
  }

  fetchFromServer = () => {
    console.log("am i here1112222lasdoiabfuibwefiubwefu");
    this.setState({ loading: true });
    AsyncStorage.getItem('userToken').then(
      (token) => {
        let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
        fetch("http://"+ address + ':8000/point/author_pointer_list/', {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token  '+ token, 
          }
        }).then(res => res.json())
        .then(res => {
          // convert to array with data field
          var con1 = res.map(function(x) {
            var c = [];
            return {data: [x]}
          });
          this.setState({
            data: [...this.state.data, ...con1],
            error: res.error || null,
            loading: false
          });
          console.log(this.state.data);
          // add data in front
        }).catch((error) => {
          // fetch error
          console.log(error)
          this.setState({ error, loading: false });
          return
        });
    }).catch((error) => {
      // storage error
      console.log(error)
      return
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <SectionList
          keyExtractor={keyExtractor}
          renderSectionHeader={renderSectionHeader}
          renderItem={renderItem1}
          sections={this.state.data}
        />
      </View>
    );
  }
}

function keyExtractor(item) {
  return item.title
}
// <Text style={styles.sectionTitle}>{section.item.pointer_date}</Text>
const renderSectionHeader = ({ section }) => {
  // console.log(section.data[0].id);
  return(
    <View style={styles.sectionContainer}>
      <Text style={styles.sectionTitle}>{section.data[0].pointer_date}</Text>
    </View>
  )
}

const renderItem = ({ item }) => <ListItem title={item.title} />
const renderItem1 = ({ item }) => <Text>{ item.title }</Text>



const styles = StyleSheet.create({
  SectionHeaderStyle:{
    backgroundColor : '#CDDC39',
    fontSize : 20,
    padding: 5,
    color: '#fff',
    borderTopWidth: 5,
    borderColor: 'black'
  },
 
  SectionListItemStyle:{
    fontSize : 15,
    padding: 5,
    color: '#000',
    backgroundColor : '#F5F5F5',
    borderTopWidth: 5,
    borderColor: 'black',
  },
  container: {
    flex: 1,
    paddingTop: appjs.statusBarHeight,
    backgroundColor: 'white',
  },
  sectionContainer: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.12)',
    backgroundColor: '#efefef',
  },
  sectionTitle: {
    color: 'black',
    fontSize: 14,
    marginBottom: 8,
    marginLeft: 16,
    marginRight: 16,
    marginTop: 24,
    opacity: 0.8,
  }
});

const SECTION = [
  { title: 'Section 1', name: '' },
  { title: 'ListItem' },
  { title: 'ListItem 3' },
  { title: 'ListItem 4' },
  { title: 'ListItem 5' },
  { title: 'ListItem 6' },
]

const SECTIONS = [
  {
    data: [
      {
        title: 'List Item 1',
      },
      {
        title: 'List Item 2',
      },
      {
        title: 'List Item 3',
      },
      {
        title: 'List Item 4',
      },
    ],
    title: 'SECTION 1',
  },
  {
    data: [
      {
        title: 'List Item 1',
      },
      {
        title: 'List Item 2',
      },
      {
        title: 'List Item 3',
      },
      {
        title: 'List Item 4',
      },
    ],
    title: 'SECTION 2',
  },
  {
    data: [
      {
        title: 'List Item 1',
      },
      {
        title: 'List Item 2',
      },
      {
        title: 'List Item 3',
      },
      {
        title: 'List Item 4',
      },
    ],
    title: 'SECTION 3',
  },
  {
    data: [
      {
        title: 'List Item 1',
      },
      {
        title: 'List Item 2',
      },
      {
        title: 'List Item 3',
      },
      {
        title: 'List Item 4',
      },
    ],
    title: 'SECTION 4',
  },
]
