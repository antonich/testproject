/* @flow */
import React, {Component} from 'react';
import createReactClass from 'create-react-class';
import { View, Text, StyleSheet, TextInput,
	TouchableOpacity, StatusBar, KeyboardAvoidingView, Platform,
		Dimensions, AsyncStorage} from 'react-native';
import PropTypes from 'prop-types';
import * as  appActions from '../../actions/index';
import {Navigation} from 'react-native-navigation';
import {connect} from 'react-redux';

var appjs = require('../../app.js');

const FBSDK = require('react-native-fbsdk');
const {
  LoginButton,
  AccessToken
} = FBSDK;

export class LoginForm extends Component {
	constructor() {
		super();
		this.state = {
			username: '',
			password: '',
			user: null
		};
	}

	render() {
		var _this = this;
		return (
			<View behavior='padding' style={ styles.container }>
				<StatusBar
					barStyle="dark-content"
				/>
				<View style={ styles.inputContainer }>
					<TextInput
						placeholder="Email"
						placeholderTextColor="grey"
						returnKeyType="next"
						autoCapitalize="none"
						underlineColorAndroid="rgba(0,0,0,0)"
						autoCorrect={false}
						style={styles.input}
						onChangeText={this.handleUsername} 
					/>

					<TextInput
						placeholder="Password"
						placeholderTextColor="grey"
						returnKeyType="go"
						secureTextEntry
						underlineColorAndroid='rgba(0,0,0,0)'
						style={styles.input}
						onChangeText = {this.handlePassword} 
					/>

				</View>

				<TouchableOpacity
					style={styles.loginButton}
					activeOpacity={0.6}
					onPress = {
              			() => this.loginPress(this.state.username, this.state.password)
					}
				>
					<Text style={styles.buttonText}>Login</Text>
				</TouchableOpacity>

				<View style={ styles.lineOutter} >
					<View style={ styles.orLine } />
					<Text style={styles.orText}>or</Text>
				</View>

				<LoginButton
					readPermissions={["email","public_profile","user_friends"]}
					onLoginFinished={
						(error, result) => {
							if (error) {
								alert("login has error: " + result.error);
							} else if (result.isCancelled) {
								alert("login is cancelled.");
							} else {
								AccessToken.getCurrentAccessToken().then(
									(data) => {
										// alert(data.accessToken.toString())
	                  					this.facebookLogin(data.accessToken.toString())
									}
								)
							}
						}
					}
					onLogoutFinished={() => alert("logout.")}
				/>

			</View>
		);
	}

	handleUsername = (text) => {
		 this.setState({ username: text })
	}

	handlePassword = (text) => {
		 this.setState({ password: text })
	}

	async changeIsLoggedIn(key, value) {
		try {
        	return await AsyncStorage.setItem(key, value);
    	} catch (error) {
        	console.error('AsyncStorage#setItem error: ' + error.message);
    	}
	}

	loginPress = (username, pass) => {
		if (pass.trim() == '' || username.trim() == '') {
			alert("error")
			return
		}
		let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
		fetch('http://'+ address +':8000/users/login/', {
		  method: 'POST',
		  headers: {
		    Accept: 'application/json',
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
		    username: username,
		    password: pass
		  }),
		}).then((response) => {
			if (response.status != 200) {
				alert("User data problem.")
				throw Error(response.status);
			} else {
				response.json().then(
					(data) => {
						this.changeIsLoggedIn('userToken', data.token);
						this.changeIsLoggedIn('isLoggedIn', 'True');
						this.props.dispatch(appActions.login());
					})
			}

		}).catch((error) => {
			return
		});
	}

	facebookLogin = (fbToken) => {
		let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
		fetch('http://'+ address + ':8000/users/social/facebook/', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				access_token: fbToken
			}),
		}).then((response) => {
			if (response.status != 200) {
				alert("Problem with login.")
				//throw Error(response.status);
			} else {
				response.json().then(
					(data) => {
						this.changeIsLoggedIn('userToken', data.token);
						this.changeIsLoggedIn('isLoggedIn', 'True');
						this.props.dispatch(appActions.login());
					})
			}
		});
	}
}

export default connect()(LoginForm);


var width = Dimensions.get('window').width;
let widthConstant = 0.7;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
		padding: 20,
		paddingTop: 120,
		// justifyContent: 'center',
		alignItems: 'center',
	},
	inputContainer: {
	},
	input: {
		width: width * widthConstant,
		height: 40,
		backgroundColor: 'white',
		marginBottom: 10,
		borderColor: 'grey',
		borderRadius: 5,
		borderWidth: 1,
		paddingHorizontal: 10,
		...Platform.select({
			ios: {
				padding: 7,
			},
			android: {
				paddingBottom: 0,
				height: 40,
				textAlignVertical: "top",
			},
		}),
	},
	loginButton: {
		width: width * widthConstant,
		backgroundColor: 'white',
		marginTop: 5,
	},
	buttonText: {
		textAlign: 'center',
		fontWeight: '700',
		borderWidth: 1,
		borderColor: '#3b3b6e',
		paddingTop: 10,
		paddingBottom: 10,
		color: '#3b3b6e'
	},
	lineOutter: {
		width: width * widthConstant,
		marginTop: 40,
		// height: 40,
		alignItems: 'center',

	},
	orLine: {
		backgroundColor: 'black',
		height: 1,
		width: width * widthConstant
	},
	orText: {
		top: -10,
		color: 'black',
		textAlign: 'center',
		padding: 10,
		paddingTop: 0,
		paddingBottom: 0,
		backgroundColor: 'white',

	},
	facebookButton: {
		marginTop: 20,
	}
});
