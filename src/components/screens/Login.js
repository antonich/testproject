import React, {Component} from 'react';
import { View, Text, StyleSheet, Image, Button, TouchableOpacity, Animated, Keyboard} from 'react-native';
import LoginForm from './LoginForm';
import SignUpForm from './SignUpForm';
import logo from './media/topPin2.png';
import {Navigation} from 'react-native-navigation';
import {connect} from 'react-redux';
import * as  appActions from '../../actions/index';


var heightToggleCell;

let IMAGE_HEIGHT = 95.9;
let IMAGE_HEIGHT_SMALL = 50.0;
let IMAGE_WIDTH = 62.70;
let IMAGE_WIDTH_SMALL = 35.0;

export default class Login extends Component<{}> {
	constructor() {
		super();
		this.state = {
			active: true
		};

		this.keyboardHeight = new Animated.Value(0);
    	this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
		this.imageWidth = new Animated.Value(IMAGE_WIDTH);
	}

	componentWillMount () {
	    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
	    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
	}

	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}

	keyboardWillShow = (event) => {
		Animated.parallel([
			Animated.timing(this.keyboardHeight, {
				duration: event.duration,
				toValue: event.endCoordinates.height,
			}),
			Animated.timing(this.imageHeight, {
				duration: event.duration,
				toValue: IMAGE_HEIGHT_SMALL,
			}),
			Animated.timing(this.imageWidth, {
				duration: event.duration,
				toValue: IMAGE_WIDTH_SMALL,
			}),
		]).start();
	};

	keyboardWillHide = (event) => {
		Animated.parallel([
		  Animated.timing(this.keyboardHeight, {
		    duration: event.duration,
		    toValue: 0,
		  }),
		  Animated.timing(this.imageHeight, {
		    duration: event.duration,
		    toValue: IMAGE_HEIGHT,
		  }),
		  Animated.timing(this.imageWidth, {
			duration: event.duration,
			toValue: IMAGE_WIDTH,
				}),
		]).start();
	};

	onLayout = (e) => {
		heightToggleCell = e.nativeEvent.layout.height;
	}

	_loginButtonPress() {
		this.setState({ active: true });
	}

	_regButtonPress() {
		this.setState({ active: false });
	}

	render() {
		return (
			<Animated.View style={[styles.wrapper, { paddingBottom: this.keyboardHeight }]}>
				<View style={ styles.topPinWrapper }>
					<Animated.Image source={logo} style={[styles.topPing, { height: this.imageHeight, width: this.imageWidth }]} />
				</View>

				<View onLayout={this.onLayout} style={ styles.toggleTopPanel }>

						<TouchableOpacity disabled={this.state.active} onPress={ this._loginButtonPress.bind(this) } 
							style={ [ styles.toggleCell, this.state.active && styles.activeCell ] }>
							 <Text> Login </Text>
						</TouchableOpacity>
						<TouchableOpacity disabled={(!this.state.active)} onPress={ this._regButtonPress.bind(this) } 
							style={ [styles.toggleCell, (!this.state.active) && styles.activeCell] }>
							 <Text> Sign up </Text>
						</TouchableOpacity>

				</View>

				<View style={ styles.panelWrapper }>
					{ this.state.active &&
						<View style={ [styles.loginPanel, this.state.active && styles.activePanel ] } >
							<LoginForm />
						</View> 
					}
					{ (!this.state.active) &&
						<View style={ [styles.regPanel, (!this.state.active) && styles.activePanel] }>
							<SignUpForm />
						</View>
					}
				</View>

		  	</Animated.View>
		);
	}
}


const yForToggleCell = 83;
const yTopPinWrapper = 47.5;

const styles = StyleSheet.create({
	wrapper: {
		backgroundColor: '#F8F8F8',
		flex: 1,
	},
	topPing: {
		width: 62.70,
		height: 95.9,
	},
	topPinWrapper: {
		alignItems: 'center',
		top: yTopPinWrapper,
		paddingBottom: 35
	},
	toggleTopPanel: {
		top: yTopPinWrapper,
		flexDirection: 'row',
	},
	toggleCell: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 10,
		paddingBottom: 10,
		borderBottomWidth: 0.5
	},
	button: {
		fontSize: 5
	},
	activeCell: {
		backgroundColor: 'white',
		borderBottomWidth: 0,
		borderTopWidth: 0.5,
		borderLeftWidth: 0.5,
		borderRightWidth: 0.5,
		borderColor: 'black'
	},
	panelWrapper: {
		top: yTopPinWrapper,
		flex: 1,
		backgroundColor: 'white'
	},
	loginPanel: {
		backgroundColor: 'white',
		height: 0
	},
	regPanel: {
		backgroundColor: 'white',
		height: 0
	},
	activePanel: {
		flex: 1
	}
});
