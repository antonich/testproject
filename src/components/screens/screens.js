import { Navigation } from 'react-native-navigation';
import Login from './Login';
import HomeTab from './HomeTab';
import UserTab from './UserTab';
import DiscoverTab from './DiscoverTab';
import PointerScreen from './screens2/PointerScreen';



export default (store, Provider) =>  {
	Navigation.registerComponent('Pointer.Login', () => Login, store, Provider);
	Navigation.registerComponent('Pointer.HomeTab', () => HomeTab, store, Provider);
	Navigation.registerComponent('Pointer.UserTab', () => UserTab, store, Provider);
	Navigation.registerComponent('Pointer.DiscoverTab', () => DiscoverTab, store, Provider);
	
	Navigation.registerComponent('Pointer.HomeTab.PointerScreen', () => PointerScreen, store, Provider);	
}