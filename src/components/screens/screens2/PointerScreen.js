/* @flow */ 

import React, {Component} from 'react';
import {StyleSheet, View, Text, Button, Alert} from 'react-native';

class PointerScreen extends Component {
  static navigationOptions = { header: null }
  constructor(props) {
    super(props);
    let to = 'hidden';
    // this.props.navigator.toggleNavBar({
    //   to,
    //   animated: false
    // });
  }

  onPushAnother = () => {
    this.props.navigator.push({
      screen: 'example.HomeTab',
      title: `Screen ${this.props.count || 1}`,
      passProps: {
      }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Pushed Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  button: {
    marginTop: 16
  }
});

export default PointerScreen;