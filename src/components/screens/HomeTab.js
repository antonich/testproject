/* @flow */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  SectionList,
  Animated,
  AsyncStorage,
  FlatList,
  ActivityIndicator,
  NavigatorIOS,
  TouchableOpacity,
  Alert
} from 'react-native';
import Navigation from 'react-native-navigation';
import {connect} from 'react-redux';

var appjs = require('../../app.js');

class FeedItem extends Component {
  constructor(props) {
      super(props);
  }

  pushScreen = () => {
    this.props.navigator.push({
      screen: 'Pointer.HomeTab.PointerScreen',
      title: 'Pointer Name',
      navigatorStyle: {
        navBarHidden: true // to hide top navbar title
      }
    });
  };

  render() {
    return(
      <View>
        <TouchableOpacity onPress={this.pushScreen} style={ styles.feedItem }>
          <Text>title : { this.props.title }</Text>
          <Text>date: { this.props.pointer_date }</Text>
          {this.props.members.map(item => {
            console.log(item);
            return (
             <Text key={ item.user.id }>User id = { item.user.username } status = {item.status}</Text>
            );
          })}
        </TouchableOpacity>
      </View>
    )
  }
}//


export default class Hometab extends Component {
  constructor(props : Object) {
      super(props);

      this.state = {
        loading: false,
        data: [],
        error: null,
        scrollY: new Animated.Value(0),
        refreshing: false,
        page: 1,
        data_ids: []
    };
  }//

  componentDidMount() {
    this.fetchFromServer();  
  }

  updateDataIds() {
    var ids = [];
    for(var i in this.state.data) {
      var obj = this.state.data[i];
      ids.push(obj["pointer"].id);
    }

    this.setState({
      data_ids: [...this.state.data_ids, ...ids]
    });

  }

  checkForNewPoints(res) {
    var new_data = [];
    for(var i in res){
      var num = res[i]["pointer"].id;
      if(this.state.data_ids.indexOf(num) == -1)
        new_data.push(res[i]);
    }
    return new_data;
  }

  fetchFromServer = () => {
    this.setState({ loading: true });
    AsyncStorage.getItem('userToken').then(
      (token) => {
        let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
        fetch("http://"+ address + ':8000/point/storyline/', {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token  '+ token, 
          }
        }).then(res => res.json())
        .then(res => this.checkForNewPoints(res))
        .then(res => {
          console.log(res);
          this.setState({
            data: [...this.state.data, ...res],
            error: res.error || null,
            loading: false,
            refreshing: false,
          });
          this.updateDataIds();
        }).catch((error) => {
          // fetch error
          console.log(error)
          this.setState({ error, loading: false, refreshing: false });
          return
        });
    }).catch((error) => {
      // storage error
      console.log(error)
      return
    });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "80%",
          backgroundColor: "#CED0CE",
          marginLeft: "10%"
        }}
      />
    );
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };// 

  handleRefresh = () => {
    this.setState({
      refreshing: true
    }, () => {
      this.fetchFromServer()
    })
  }

  render() {
    return (
      <View style={ styles.container } >
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <FeedItem
              title={ item.pointer.title }
              subtitle={ item.pointer.id }
              pointer_date={ item.pointer.pointer_date }
              members={ item.members }
              navigator={this.props.navigator}
            />

          )}
          keyExtractor={item => item.pointer.id}
          ListFooterComponent={this.renderFooter}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
        />
      </View>
    );
  }
}//

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: appjs.statusBarHeight,
    backgroundColor: 'white',
  },
  feedItem: {
    padding: 10,
    margin: 5,
    borderWidth: 1,
    backgroundColor: '#FFF000',
    borderRadius: 5,
    marginBottom: 3
  },
  container1: {
    flex: 1,
  },
  scene: {
    padding: 10,
    paddingTop: 74,
    flex: 1,
  }
});