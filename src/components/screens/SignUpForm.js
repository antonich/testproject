import React, {Component} from 'react';
import { View, Text, StyleSheet, TextInput,
	TouchableOpacity, StatusBar, KeyboardAvoidingView, Platform,
		Button, Dimensions, AsyncStorage} from 'react-native';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';
import * as  appActions from '../../actions/index';
import {Navigation} from 'react-native-navigation';
import {connect} from 'react-redux';

var appjs = require('../../app.js');

export class SignUpForm extends Component {
	constructor() {
		super();
		this.state = {
			username: '',
			password1: '',
			email: '',
			password2: '',
			user: null
		};
	}

	handleUsername = (text) => {
		 this.setState({ username: text })
	}

	handlePassword1 = (text) => {
		 this.setState({ password1: text })
	}

	handlePassword2 = (text) => {
		 this.setState({ password2: text })
	}

	handleEmail = (text) => {
		 this.setState({ email: text })
	}

	_passCheck(pass1, pass2) {
		//console.log("pass1 =%s, pass2 =%s", pass1, pass2);
		if (pass1 != pass2) return true
		else return false
	}

	signPress = (username, email, pass1, pass2) => {
		if (pass1.trim() == '' || username.trim() == '' || pass2.trim() == '' || email.trim() == '' || this._passCheck(pass1, pass2)) {
			alert("Fields error.")
			return
		}
		///// ---- ALSO CHECK FOR EMAIL !
		let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
		fetch('http://'+address+':8000/users/register/', {
		  method: 'POST',
		  headers: {
		    Accept: 'application/json',
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
		    username: username,
		    password: pass1,
		    email: email
		  }),
		}).then((response) => {
				if (response.status != 201) {
					alert("Problem with login.")
					// throw Error(response.status);
				} else {
    				response.json().then(
						this.loginPress(username, pass1)
    					// (data) => {
    					// 	console.log("am i here??")
							// loginPress(username, pass1)
							// this.props.dispatch(appActions.login());
					)
    			}	
			}).catch((error) => {
				console.log(error)
				return
		    });
	}

	async changeIsLoggedIn(key, value) {
		try {
        	return await AsyncStorage.setItem(key, value);
    	} catch (error) {
        	console.error('AsyncStorage#setItem error: ' + error.message);
    	}
	}

	loginPress = (username, pass) => {
		console.log("and here also should be!")
		let address = ( appjs.debug == true ? '192.168.0.102' : 'localhost');
		fetch('http://'+address+':8000/users/login/', {
		  method: 'POST',
		  headers: {
		    Accept: 'application/json',
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
		    username: username,
		    password: pass
		  }),
		}).then((response) => {
			if (response.status != 200) {
				alert("User data problem.")
				throw Error(response.status);
			} else {
				response.json().then(
					(data) => {
						console.log(data.token); // token returns here
						this.changeIsLoggedIn('userToken', data.token);
						this.changeIsLoggedIn('isLoggedIn', 'True');
						this.props.dispatch(appActions.login());
					})
			}

		}).catch((error) => {
			return
		});
	}

	render() {
		var _this = this;
		return (
			<KeyboardAvoidingView behavior="padding" style={ styles.container }>

				<StatusBar
					barStyle="dark-content"
				/>
				<View style={ styles.inputContainer }>

					<TextInput
						placeholder="Username"
						placeholderTextColor="grey"
						returnKeyType="next"
						autoCapitalize="none"
						underlineColorAndroid='rgba(0,0,0,0)'
						autoCorrect={false}
						style={styles.input}
						onChangeText = {this.handleUsername} />

					<TextInput
						placeholder="Email"
						placeholderTextColor="grey"
						keyboardType="email-address"
						returnKeyType="next"
						autoCapitalize = 'none'
						underlineColorAndroid='rgba(0,0,0,0)'
						style={styles.input}
						onChangeText = {this.handleEmail} />

					<TextInput
						placeholder="Password"
						placeholderTextColor="grey"
						returnKeyType="next"
						secureTextEntry
						autoCapitalize="none"
						underlineColorAndroid='rgba(0,0,0,0)'
						autoCorrect={false}
						style={styles.input}
						onChangeText = {this.handlePassword1} />

					<TextInput
						placeholder="Repeat password"
						placeholderTextColor="grey"
						returnKeyType="go"
						secureTextEntry
						autoCapitalize="none"
						underlineColorAndroid='rgba(0,0,0,0)'
						autoCorrect={false}
						style={styles.input}
						onChangeText = {this.handlePassword2} />

				</View>

				<TouchableOpacity
					style={styles.loginButton}
					activeOpacity={0.6}
					onPress = {
              			() => this.signPress(this.state.username, this.state.email, this.state.password1, this.state.password2)
					}
				>
					<Text style={styles.buttonText}>Login</Text>
				</TouchableOpacity>
				
				<View style={ styles.lineOutter} >
					<View style={ styles.orLine } />
					<Text style={styles.orText}>or</Text>
				</View>

			</KeyboardAvoidingView>
		);
	}
}
//

export default connect()(SignUpForm);

var width = Dimensions.get('window').width;
let widthConstant = 0.7;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
		padding: 20,
		paddingTop: 40,
		alignItems: 'center',
	},
	inputContainer: {
	},
	input: {
		width: width * widthConstant,
		height: 40,
		backgroundColor: 'white',
		marginBottom: 10,
		borderColor: 'grey',
		borderRadius: 5,
		borderWidth: 1,
		paddingHorizontal: 10,
		...Platform.select({
			ios: {
				padding: 7,
			},
			android: {
				paddingBottom: 0,
				height: 40,
				textAlignVertical: "top",
			},
		}),
	},
	loginButton: {
		width: width * widthConstant,
		backgroundColor: 'white',
		marginTop: 5,
	},
	buttonText: {
		textAlign: 'center',
		fontWeight: '700',
		borderWidth: 1,
		borderColor: '#3b3b6e',
		paddingTop: 10,
		paddingBottom: 10,
		color: '#3b3b6e'
	},
	lineOutter: {
		width: width * widthConstant,
		marginTop: 40,
		// height: 40,
		alignItems: 'center',

	},
	orLine: {
		backgroundColor: 'black',
		height: 1,
		width: width * widthConstant
	},
	orText: {
		top: -10,
		color: 'black',
		textAlign: 'center',
		padding: 10,
		paddingTop: 0,
		paddingBottom: 0,
		backgroundColor: 'white',

	},
	facebookButton: {
		marginTop: 20,
	}
});
