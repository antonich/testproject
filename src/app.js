/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AsyncStorage
} from 'react-native';
import Login from './components/screens/Login.js';
import {createStore, applyMiddleware, combineReducers} from "redux";
import {Provider} from "react-redux";
import { Navigation } from 'react-native-navigation';
import registerScreens from './components/screens/screens.js';
import * as reducers from "./reducers/index";
import * as appActions from "./actions/index";
import thunk from "redux-thunk";
import SplashScreen from 'react-native-splash-screen';
import Icon from 'react-native-vector-icons/Ionicons';

// images 
import placeholders from './img/placeholders.pdf';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);
registerScreens(store, Provider);


export default class App extends Component {
	constructor(props) {
		super(props);
		this._consoleUserToken();
		store.subscribe(this.onStoreUpdate.bind(this));
		const isLoggedIn = AsyncStorage.getItem('isLoggedIn').then(
			(value) => {
				if (value == "True") {
					SplashScreen.hide();
					store.dispatch(appActions.login());
				} else {
					SplashScreen.hide();
					store.dispatch(appActions.appInitialized());
				}
		}).catch((error) => {
			console.log(error)
			return
		});

	}

	componentDidMount() {
	    SplashScreen.hide();
	}

	onStoreUpdate() {
		// console.log("siema2");
		let {root} = store.getState().root;

		if (this.currentRoot != root) {
			this.currentRoot = root;
			this.startApp(root);
		}
	}

	_consoleUserToken() {
	const isLoggedIn = AsyncStorage.getItem('userToken').then(
	  (value) => {
	    console.log('User token = %s', value);
	}).catch((error) => {
	  console.log(error)
	  return
	});
	}

  startApp(root) {
    switch (root) {
        case 'login':
			Navigation.startSingleScreenApp({
			    screen: {
			    screen: 'Pointer.Login', // unique ID registered with Navigation.registerScreen
			    title: 'Welcome', // title of the screen as appears in the nav bar (optional)
			    navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
			    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
			    },
			});
            return;
              
        case 'after-login':
			Navigation.startTabBasedApp({
			    tabs: [
			    {
			        label: 'Pointer',
			        screen: 'Pointer.HomeTab',
			        icon: require('./img/placeholder_24.png'),
			        selectedIcon: require('./img/placeholder_24.png'),
			        title: 'Home',
			        overrideBackPress: false,
			        navigatorStyle: {}
			    },

			    {
			        label: 'Discover',
			        screen: 'Pointer.DiscoverTab',
			        icon: require('./img/worldwide_24.png'),
			        selectedIcon: require('./img/worldwide_24.png'),
			        title: 'Discover',
			        navigatorStyle: {navBarHidden: true}
			    },

			    {
			        label: 'Create',
			        screen: 'Pointer.UserTab',
			        icon: require('./img/plus-button_24.png'),
			        selectedIcon: require('./img/plus-button_24.png'),
	              	iconInsets: { // add this to change icon position (optional, iOS only).
				    },
			        title: 'Hey',
			        navigatorStyle: {}
			    },

			    {
			        label: 'Notifications',
			        screen: 'Pointer.UserTab',
			        icon: require('./img/notification_24.png'),
			        selectedIcon: require('./img/notification_24.png'),
			        title: 'Hey',
			        navigatorStyle: {}
			    },

			    {
			        label: 'Personal',
			        screen: 'Pointer.UserTab',
			        icon: require('./img/avatar_24.png'),
			        selectedIcon: require('./img/avatar_24.png'),
			        title: 'Hey',
			        navigatorStyle: {}
			    }
			   
			    ],
			    animationType: 'slide-down',
			    tabsStyle: { // optional, add this if you want to style the tab bar beyond the defaults
					// tabBarButtonColor: '#ffff00', // optional, change the color of the tab icons and text (also unselected). On Android, add this to appStyle
					// tabBarSelectedButtonColor: '#ff9900', // optional, change the color of the selected tab icon and text (only selected). On Android, add this to appStyle
					tabBarBackgroundColor: 'white', // optional, change the background color of the tab bar
					tabBarTranslucent: true
					// initialTabIndex: 1, // optional, the default selected bottom tab. Default: 0. On Android, add this to appStyle
  				}
			});
            return;

        default: 
        	console.log("Not Root Found");
        }
    }
}

module.exports.debug = false;
module.exports.statusBarHeight = 20;
